import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Platform} from 'react-native';
import Colors from "../constants/Colors";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

const MyTabBar = ({state, descriptors, navigation}) => {
    return (
        <View style={{flexDirection: 'row'}}>
            {state.routes.map((route, index) => {
                const {options} = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                        ? options.title
                        : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                let iconName = "home";
                if(route.name === 'User'){
                    iconName = "user-cog";
                } else if (route.name === 'Settings') {
                    iconName = "sliders-h";
                }


                return (
                    <TouchableOpacity
                        accessibilityRole="button"
                        accessibilityStates={isFocused ? ['selected'] : []}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={styles.wrapper}
                        key={index.toString()}
                    >
                        <View style={{
                            ...styles.subWrapper, ...{
                                backgroundColor: isFocused ? Colors.WHITE : Colors.WHITE,
                                borderTopColor: Colors.black,
                                borderTopWidth: isFocused ? 1 : 0
                            }
                        }}>
                            <FontAwesomeIcon name={iconName}
                                             size={22}
                                             solid
                                             color={isFocused ? Colors.TRC_PRIMARY : Colors.TRC_SECONDARY}/>
                            <View style={styles.decoraterWrapper} />
                        </View>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        height: Platform.OS === 'ios' ? 60 : 50,
    },
    subWrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    decoraterWrapper: {
        width: '100%',
        height: Platform.OS === 'ios' ? 70 : 60,
        position: 'absolute',
        borderTopWidth: 1,
        borderTopColor: Colors.white,
        borderRadius: Platform.OS === 'ios' ? 35 : 30,
    },
    text: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    }
});

export default MyTabBar;
