import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Colors from "../constants/Colors";


class MyTouchableOpacityButton extends Component {

    render() {
        return (
            <TouchableOpacity onPress={() => {
                this.props.onPress()
            }}>
                <View style={{...styles.wrapper, ...this.props.style}}>
                    <Text style={{...styles.textStyle, ...this.props.textStyle}}>
                        {this.props.buttonText}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: Colors.BLACK,
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.5,
        elevation: 5
    },
    textStyle: {
        fontSize: 24,
        color: Colors.WHITE,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default MyTouchableOpacityButton;
