import React, {Component} from 'react';
import {View, TextInput, StyleSheet, Text} from 'react-native';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import Colors from "../constants/Colors";

class MyTextInput extends Component {

    state = {
        text: null
    }

    componentDidMount() {
        this.setState({
            text: this.props.value
        })
    }

    getSnapshotBeforeUpdate(prevProps: Readonly<P>, prevState: Readonly<S>): SS | null {
        return prevProps.value !== prevState.value;
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        return snapshot;
    }

    render() {
        return (
            <View style={styles.textInputWrapper}>
                <FontAwesomeIcon name={this.props.icon} size={this.props.iconSize} style={{width: '10%'}}
                                 solid={this.props.isIconSolid} color={Colors.WHITE}/>
                <TextInput
                    autoCorrect={this.props.autoCorrect}
                    autoCapitalize={this.props.autoCapitalize}
                    autoCompleteType={this.props.autoCompleteType}
                    style={{...styles.textInput, ...this.props.style}}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={this.props.placeholderTextColor}
                    keyboardType={this.props.keyboardType}
                    onChangeText={(text) => {
                        this.setState({text});
                        this.props._setState(this.props.stateKey, text);
                    }}
                    value={this.state.text}
                    secureTextEntry={this.props.secureTextEntry}
                    maxLength={this.props.maxLength ? this.props.maxLength : 50}
                />
            </View>
        )
    }

}


const styles = StyleSheet.create({
    textInputWrapper: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Colors.METALIC_GRAY,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50
    },
    textInput: {
        width: '90%',
        height: 40,
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.WHITE
    },

});

export default MyTextInput;
