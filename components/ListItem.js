import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import Colors from "../constants/Colors";

class ListItem extends Component {

    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigateToPage(this.props.page)}>
                <View style={styles.wrapper}>
                    <Text style={styles.contentText}>{this.props.title}</Text>
                    <FontAwesomeIcon name="chevron-right" size={50} color={Colors.METALIC_GRAY} style={{width: '10%'}}/>
                </View>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 80,
        borderRadius: 5,
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 1,
            height: 2
        },
        backgroundColor: Colors.TRC_PRIMARY,
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        flexDirection: 'row',
        marginBottom: 20
    },
    contentText: {
        color: Colors.WHITE,
        fontSize: 16,
        fontWeight: 'bold',
        width: '90%'
    }
});

export default ListItem;
