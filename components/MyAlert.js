import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Modal, Dimensions, Animated, Easing} from 'react-native';
import FontawesomeIcon from 'react-native-vector-icons/FontAwesome5';
import Colors from "../constants/Colors";

class MyAlert extends Component {

    constructor(props) {
        super(props);
        this.modalScale = new Animated.Value(0);
    }

    state = {
        isModalVisible: false
    }

    componentDidMount() {
        this.setState({
            isModalVisible: this.props.isModalVisible
        })
    }

    _startAnimate = () => {
        //console.log('_startAnimate');
        Animated.sequence([
            Animated.delay(500),
            Animated.timing(this.modalScale, {
                toValue: 1,
                duration: 150,
                easing: Easing.linear(),
                useNativeDriver: true
            })
        ]).start();

    }

    getSnapshotBeforeUpdate(prevProps: Readonly<P>, prevState: Readonly<S>): SS | null {
        this._startAnimate();
        return prevProps.isModalVisible !== this.props.isModalVisible
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        return snapshot;
    }

    render() {

        let buttons = null;

        if (this.props.buttons.length > 0) {
            buttons = this.props.buttons.map((button, index) => {
                let wrapperStyle = styles.closeButtons;
                if (index > 0) {
                    wrapperStyle = {
                        ...wrapperStyle, ...{
                            borderLeftWidth: 1,
                            borderLeftColor: Colors.METALIC_GRAY
                        }
                    };
                }
                return (
                    <View style={wrapperStyle} key={index}>
                        <TouchableOpacity
                            onPress={() => {
                                button.onPress()
                            }}>

                            <Text style={styles.closeButtonText}>{button.text}</Text>

                        </TouchableOpacity>
                    </View>
                );
            });
        }


        return (
            <>
                <Modal
                    transparent={true}
                    visible={this.props.isModalVisible}
                >
                    <View style={{width: '100%', height: '100%', backgroundColor: 'rgba(52, 52, 52, 0.6)'}}>
                        <Animated.View style={{
                            ...styles.wrapper,
                            ...{
                                transform: [
                                    {
                                        scale: this.modalScale.interpolate({
                                            inputRange: [0, .5, 1],
                                            outputRange: [1, 1.15, 1]
                                        })
                                    }
                                ]
                            }
                        }}>
                            <View style={styles.content}>
                                <View style={styles.titleWrapper}>
                                    <FontawesomeIcon
                                        name={this.props.alertIcon}
                                        size={22}
                                        color={this.props.alertIconColor}/>
                                    <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 15}}>{this.props.title}</Text>
                                </View>
                                <View style={styles.message}>
                                    <Text style={{fontSize: 16, marginLeft: 15}}>{this.props.message}</Text>
                                </View>

                                <View style={styles.buttonsWrapper}>
                                    {buttons}
                                </View>
                            </View>
                        </Animated.View>
                    </View>
                </Modal>

            </>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 50,
        shadowColor: Colors.BLACK,
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.5,
        elevation: 5
    },
    content: {
        width: '100%',
        height: Dimensions.get('window').height / 5,
        borderRadius: 10,
        backgroundColor: Colors.WHITE,
    },
    titleWrapper: {
        width: '100%',
        height: '25%',
        borderBottomWidth: 1,
        borderBottomColor: Colors.METALIC_GRAY,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 20,
        flexDirection: 'row'
    },
    message: {
        width: '100%',
        height: '50%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: Colors.METALIC_GRAY,
    },
    buttonsWrapper: {
        width: '100%',
        height: '25%',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
    },
    closeButtons: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    closeButtonText: {
        width: '100%',
        color: Colors.LIGHT_BLUE,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
    }
});

export default MyAlert;
