import axios from 'axios';
import Common from "./constants/Common";

let headers = {
    'Content-Type': 'application/json',
    'x-auth-token': ''
}

const axiosGET = async (url, header = {}) => {
    if (Object.keys(header).length > 0) {
        headers = {...headers, ...header};
    }
    return await axios.get(`${Common.BACKEND_URL}${url}`, {headers});
}


const axiosPOST = async (url, data, header = {}) => {
    if (Object.keys(header).length > 0) {
        headers = {...headers, ...header};
    }
    return await axios.get(`${Common.BACKEND_URL}${url}`, data, {headers});
}

export default {axiosGET, axiosPOST};
