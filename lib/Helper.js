export default class Helper {
    static isFalsy = (data) => {
        return data === '' || data === null || data === undefined;
    }
}
