export default {
    TURCOM_BG_WITH_LOGO: require('./turcom-bg-with-logo.png'),
    TURCOM_BG_WITHOUT_LOGO: require('./turcom-bg-without-logo.png'),
    TURCOM_LOGO_WITH_COMPANYNAME: require('./turcom-logo-white.png'),
    TURCOM_LOGO_WITH_SMALL: require('./turcom-logo-white-small.png'),
    AUTH_SCREEN_BG_WITH_LOGO: require('./auth-bg.png'),
    AUTH_SCREEN_BG_WITHOUT_LOGO: require('./auth-bg-without-logo.png'),
    TRC_NFC_GREEN: require('./trc-nfc-green.png'),
    TRC_NFC_GREEN_V2: require('./trc-nfc-green-v2.png'),
}
