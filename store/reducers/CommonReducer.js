import React from 'react';
import {Dimensions} from 'react-native';

const initialState = {
    token: null,
    screenWidth: Dimensions.get('window').width,
    screenHeight: Dimensions.get('window').height,
    companyUrl: null,
    role: true,
    name: null,
    userId: null,
    userName: null,
    companyInfo: null,
    socket: null
};

const commonReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_COMPANY_INFO':
            return {...state, ...{companyInfo: action.payload}};
        case 'SET_COMPANY_URL':
            return {...state, ...{companyUrl: action.payload}};
        case 'SET_ROLE':
            return {...state, ...{role: action.payload}};
        case 'SET_NAME':
            return {...state, ...{name: action.payload}};
        case 'SET_USER_NAME':
            return {...state, ...{userName: action.payload}};
        case 'SET_USER_ID':
            return {...state, ...{userId: action.payload}};
        case 'SET_SOCKET':
            return {...state, ...{socket: action.payload}};
        default:
            return state;
    }
};

export default commonReducer;
