import React, {Component} from 'react';
import {View, Text, StyleSheet, ImageBackground, Image, Platform, Dimensions} from 'react-native';
import Images from '../constants/images/Images';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');
const {width: LOGO_IMAGE_WIDTH, height: LOGO_IMAGE_HEIGHT} = Image.resolveAssetSource(Images.TURCOM_LOGO_WITH_COMPANYNAME);

class SplashScreen extends Component {

    componentDidMount() {
    }

    render() {
        return (

            <View style={styles.wrapper}>
                <ImageBackground source={Images.TURCOM_BG_WITH_LOGO} style={styles.backgroundImage}>
                    {/*<Image source={Images.TURCOM_LOGO_WITH_COMPANYNAME} style={styles.logoImage}/>*/}
                </ImageBackground>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    logoImage: {
        width: LOGO_IMAGE_WIDTH,
        height: LOGO_IMAGE_HEIGHT,
        resizeMode: "contain",
        justifyContent: "center",
        marginTop: ((SCREEN_HEIGHT - LOGO_IMAGE_HEIGHT) / 2)
    }
});

export default SplashScreen;
