import React, {Component} from 'react';
import {SafeAreaView, ScrollView, View, Text, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import Colors from "../constants/Colors";
import ListItem from "../components/ListItem";

class ListPage extends Component {

    state={
        lists: []
    }

    componentDidMount() {
        const lists = [
            {
                title: "İki parmağı aynı anda dokunma seansı",
                page: "twoFingers"
            },
            {
                title: "Üç parmağı aynı anda dokunma seansı",
                page: "threeFingers"
            },
            {
                title: "Dört parmağı aynı anda dokunma seansı",
                page: "fourFingers"
            },
            {
                title: "Baş parmak ve serçe parmak ile sürükleme",
                page: "swipeBythumbAndLittleFinger"
            },
            {
                title: "İşaret parmağı ve Orta parmak ile yukarı sürükleme",
                page: "swipeToUpByForefingerAndMiddleFinger"
            },
            {
                title: "Orta üç parmak ile aşağı sürükleme",
                page: "swipeToDownByForefingerAndMiddleFinger"
            },
            {
                title: "Orta üç parmak ile yukarı sürükleme",
                page: "swipeToUpByMiddleThreeFingers"
            },
            {
                title: "Orta üç parmak ile aşağı sürükleme",
                page: "swipeToDownByMiddleThreeFingers"
            }
        ];

        this.setState({
            lists
        });

/*        this.props.navigation.setOptions({
            headerLayoutPreset: 'center',
            headerTitleStyle: {
                textAlign: 'center',
                width: Dimensions.get('window').width,
                color: Colors.white,
            }
        });*/
    }

    _navigateToPage = (page) => {
        this.props.navigation.navigate(page);
    }

    render() {

        let lists = this.state.lists.map((list, index) => {
            return (
                <ListItem
                    key={index}
                    title={list.title}
                    page={list.page}
                    navigateToPage={this._navigateToPage}
                />
            );
        });


        return (

            <>
                <View style={styles.wrapper}>
                    <ScrollView>
                        {lists}
                    </ScrollView>
                </View>

            </>

        );
    }

}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: '100%',
        backgroundColor: Colors.TRC_SECONDARY,
        paddingTop: 15,
        paddingHorizontal: 15
    }
});

export default ListPage;
