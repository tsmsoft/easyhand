import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
    Animated,
    Easing,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import Images from '../constants/images/Images';
import Colors from "../constants/Colors";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MyTextInput from "../components/MyTextInput";
import MyTouchableOpacityButton from "../components/MyTouchableOpacityButton";
import MyAxios from "../MyAxios";
import Helper from "../lib/Helper";
import MyAlert from "../components/MyAlert";
import {connect} from 'react-redux';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

//const {width: TRC_NFC_LOGO_IMAGE_WIDTH, height: TRC_NFC_LOGO_IMAGE_HEIGHT} = Image.resolveAssetSource(Images.TRC_NFC_GREEN_V2);

class AuthScreen extends Component {

    constructor(props) {
        super(props);
        this.trcNfcLogoTranslateValue = new Animated.Value(0);
        this.trcNfcLogoScaleValue = new Animated.Value(1.25);
        this.formContentOpacity = new Animated.Value(0);
        this.trcTurcomLogoValue = new Animated.Value(0);
    }

    state = {
        name: null,
        email: null,
        password: null,
        tckn: null,
        mobilePhone: null,
        isPasswordReminder: false,
        alertIcon: null,
        alertIconColor: null,
        message: null,
        isModalVisible: false,
        alertTitle: null

    }

    componentDidMount() {
        this._animateTrcNfcLogo('2000');
        this._animateTurcomLogo('1000');
    }

    _animateTrcNfcLogo = (duration, value = 1) => {
        Animated.sequence([
            Animated.delay(1500),
            Animated.parallel([
                Animated.timing(this.trcNfcLogoTranslateValue, {
                    toValue: value,
                    duration: duration,
                    easing: Easing.linear(),
                    useNativeDriver: true
                }),
                Animated.timing(this.trcNfcLogoScaleValue, {
                    toValue: 1,
                    duration: duration,
                    easing: Easing.linear(),
                    useNativeDriver: true
                })
            ]),
            Animated.delay(100),

            Animated.timing(this.formContentOpacity, {
                toValue: value,
                duration: 1500,
                easing: Easing.linear(),
                useNativeDriver: true
            }),
        ]).start();

    }

    _animateTurcomLogo = (duration, value = 1) => {
        this.trcTurcomLogo = Animated.timing(this.trcTurcomLogoValue, {
            toValue: value,
            duration: duration,
            easing: Easing.linear(),
            useNativeDriver: true
        }).start();
    }

    _setState = (key, value) => {
        this.setState({
            [key]: value
        })
    }

    _register = () => {
        if (Helper.isFalsy(this.state.name)) {
            this.setState({
                alertTitle: "Hata",
                alertIcon: "exclamation-triangle",
                alertIconColor: Colors.RED,
                message: "Lütfen geçerli ad soyad giriniz!",
                isModalVisible: true
            });
            return false;
        }

        if (Helper.isFalsy(this.state.tckn) || this.state.tckn.length < 11) {
            this.setState({
                alertTitle: "Hata",
                alertIcon: "exclamation-triangle",
                alertIconColor: Colors.RED,
                message: "Lütfen geçerli TC kimlik numarası giriniz!",
                isModalVisible: true
            });
            return false;
        }

        if (Helper.isFalsy(this.state.mobilePhone) || this.state.mobilePhone.length < 10) {
            this.setState({
                alertTitle: "Hata",
                alertIcon: "exclamation-triangle",
                alertIconColor: Colors.RED,
                message: "Lütfen geçerli telefon numarası giriniz!",
                isModalVisible: true
            });
            return false;
        }

        this.props.setToken('abcdefg');
    }

    _passwordRemind = () => {
        alert('Password Remind')
    }

    _passwordReminderPage = () => {
        this.setState({
            isPasswordReminder: !this.state.isPasswordReminder
        })
    }

    _closeAlert = () => {
        this.setState({
            isModalVisible: false
        })
    }

    render() {
        return (
            <KeyboardAvoidingView behaivor="padding">
                <View style={styles.wrapper}>
                    <Animated.View style={{
                        ...styles.headerLogo,
                        ...{
                            transform: [
                                {
                                    translateY: this.trcNfcLogoTranslateValue.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [0, -300],
                                    }),
                                },
                                {
                                    scale: this.trcNfcLogoScaleValue
                                },
                            ],
                        }
                    }}>
                        {/*<Image source={Images.TRC_NFC_GREEN_V2} style={styles.trcNfcLogoImage}/>*/}
                        <FontAwesomeIcon name="hand-spock" size={40} color={Colors.WHITE} solid/>
                        <Text style={styles.easyHandText}>Easyhand</Text>
                    </Animated.View>

                    <View style={styles.formWrapper}>

                        <View style={styles.formContent}>

                            <Animated.View style={{
                                opacity: this.formContentOpacity.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, 1]
                                })
                            }
                            }>
                                <MyTextInput
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    autoCompleteType="off"
                                    placeholder="Hasta Adı Soyadı"
                                    placeholderTextColor={Colors.WHITE}
                                    keyboardType="default"
                                    icon="user"
                                    iconSize={18}
                                    isIconSolid={false}
                                    secureTextEntry={false}
                                    value={this.state.text}
                                    _setState={this._setState}
                                    stateKey="name"
                                />

                                <MyTextInput
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    autoCompleteType="off"
                                    placeholder="TCKN"
                                    placeholderTextColor={Colors.WHITE}
                                    keyboardType="number-pad"
                                    icon="id-card"
                                    iconSize={18}
                                    isIconSolid={false}
                                    secureTextEntry={false}
                                    value={this.state.text}
                                    _setState={this._setState}
                                    stateKey="tckn"
                                    maxLength={11}
                                />

                                <MyTextInput
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    autoCompleteType="off"
                                    placeholder="5xxxxxxxxx"
                                    placeholderTextColor={Colors.WHITE}
                                    keyboardType="phone-pad"
                                    icon="mobile-alt"
                                    iconSize={18}
                                    isIconSolid={false}
                                    secureTextEntry={false}
                                    value={this.state.text}
                                    _setState={this._setState}
                                    stateKey="mobilePhone"
                                    maxLength={10}
                                />

                                {
                                    !this.state.isPasswordReminder ?
                                        <>
                                            {/*<MyTextInput
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            autoCompleteType="off"
                                            placeholder="Şifre"
                                            placeholderTextColor={Colors.WHITE}
                                            keyboardType="default"
                                            icon="lock"
                                            iconSize={18}
                                            isIconSolid={false}
                                            secureTextEntry={true}
                                            value={this.state.password}
                                            _setState={this._setState}
                                            stateKey="password"
                                        />*/}

                                            <MyTouchableOpacityButton
                                                buttonText="Kaydet"
                                                style={{backgroundColor: Colors.TRC_PRIMARY}}
                                                onPress={this._register}

                                            />
                                        </>

                                        :

                                        <>
                                            <MyTouchableOpacityButton
                                                buttonText="Şifreyi Sıfırla"
                                                style={{backgroundColor: Colors.TRC_PRIMARY}}
                                                onPress={this._passwordRemind}

                                            />
                                        </>
                                }


                                {/*{
                                !this.state.isPasswordReminder ?
                                    <View style={styles.underLoginButton}>
                                        <TouchableOpacity
                                            onPress={() => this._passwordReminderPage()}>
                                            <Text style={styles.passwordReminderText}>Şifremi unuttum!</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={styles.underLoginButton}>
                                        <TouchableOpacity
                                            onPress={() => this._passwordReminderPage()}>
                                            <Text style={styles.passwordReminderText}>Oturum Aç!</Text>
                                        </TouchableOpacity>
                                    </View>
                            }*/}

                                <View style={styles.socialMedia}>
                                    <View style={styles.socialMediaIconWrapper}>
                                        <FontAwesomeIcon name="google" color={Colors.TRC_PRIMARY} size={20}/>
                                    </View>
                                    <View style={styles.socialMediaIconWrapper}>
                                        <FontAwesomeIcon name="facebook-f" color={Colors.TRC_PRIMARY} size={20}/>
                                    </View>
                                    <View style={styles.socialMediaIconWrapper}>
                                        <FontAwesomeIcon name="twitter" color={Colors.TRC_PRIMARY} size={20}/>
                                    </View>
                                </View>


                            </Animated.View>

                        </View>

                    </View>


                    <Animated.View style={{
                        ...styles.bottomLogo,
                        ...{
                            opacity: 1
                        }
                    }}>
                        {/*<Image source={Images.TURCOM_LOGO_WITH_SMALL} style={styles.turcomLogoImage}/>*/}
                    </Animated.View>


                    <MyAlert
                        title={this.state.alertTitle}
                        alertIcon={this.state.alertIcon}
                        alertIconColor={this.state.alertIconColor}
                        message={this.state.message}
                        isModalVisible={this.state.isModalVisible}
                        buttons={[
                            {text: 'Tamam', onPress: this._closeAlert}
                        ]}
                    />


                </View>
            </KeyboardAvoidingView>
        );
    }

}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.TRC_SECONDARY
    },
    formWrapper: {
        width: '100%',
        height: '100%',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30
    },
    formContent: {
        width: '100%',
        height: 'auto',
        borderWidth: 0
    },
    backgroundImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    headerLogo: {
        position: 'absolute',
        marginTop: ((SCREEN_HEIGHT - (SCREEN_HEIGHT * 0.1)) / 2),
        justifyContent: 'center',
        alignItems: 'center'
    },
    easyHandText: {
        fontSize: 40,
        fontWeight: 'bold',
        color: Colors.WHITE
    },
    bottomLogo: {
        position: 'absolute',
        bottom: 10
    },
    trcNfcLogoImage: {
        width: SCREEN_WIDTH * 0.8,
        height: SCREEN_HEIGHT * 0.1,
        resizeMode: "contain",
        justifyContent: "center",
    },
    turcomLogoImage: {
        width: SCREEN_WIDTH * 0.8,
        height: SCREEN_HEIGHT * 0.07,
        resizeMode: "contain",
        justifyContent: "center",
    },
    underLoginButton: {
        width: '100%',
        height: 20,
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    passwordReminderText: {
        fontSize: 18,
        color: Colors.WHITE
    },
    socialMedia: {
        width: '100%',
        height: 25,
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: 40,
        flexDirection: 'row',
        paddingHorizontal: 50
    },
    socialMediaIconWrapper: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: Colors.TRC_PRIMARY,
        justifyContent: 'center',
        alignItems: 'center',

    },

});

const mapStateToProps = (state) => {
    return {
        token: state.authReducer.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => {
            dispatch({type: 'SET_TOKEN', payload: token});
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);
