import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MyAlert from "../components/MyAlert";
import Colors from "../constants/Colors";

class FingerButtonsScreen extends Component {

    state={
        alertIcon: null,
        alertIconColor: null,
        message: null,
        isModalVisible: false,
        alertTitle: null
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                alertTitle: "Açıklama",
                alertIcon: "info",
                alertIconColor: Colors.RED,
                message: "Lütfen bir butonu işaret parmağınız, diğer butonu serçe parmağınız ile tutup yanyana getiriniz!",
                isModalVisible: true,
            })
        }, 1000)
    }

    _closeAlert = () => {
        this.setState({
            isModalVisible: false
        })
    }

    render() {
        return (
            <>
                <View style={styles.wrapper}>




                </View>


                <MyAlert
                    title={this.state.alertTitle}
                    alertIcon={this.state.alertIcon}
                    alertIconColor={this.state.alertIconColor}
                    message={this.state.message}
                    isModalVisible={this.state.isModalVisible}
                    buttons={[
                        {text: 'Tamam', onPress: this._closeAlert}
                    ]}
                />

            </>
        );
    }

}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: '100%'
    }
});

export default FingerButtonsScreen;
