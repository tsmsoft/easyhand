import React, {Component} from 'react';
import {Alert, Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import MyTabBar from "./components/MyTabBar";
import HomeScreen from "./screens/HomeScreen";
import Colors from "./constants/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import SplashScreen from "./screens/SplashScreen";
import AuthScreen from "./screens/AuthScreen";
import {connect} from 'react-redux';
import reduxFunctions from "./store/dispatchActions";
import ListPage from "./screens/ListPage";
import FingerButtonsScreen from "./screens/FingerButtonsScreen";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

class Navigation extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        isLoading: true,
        isRegistered: false
    }

    async componentDidMount() {

        try {
            //userToken = await AsyncStorage.getItem('userToken');
            console.log(this.props.token, 'userToken on Navigation.js');
            if (this.props.token) {
                this.setState({
                    isLoading: false,
                    isRegistered: true
                })
            } else {
                setTimeout(() => {
                    this.setState({
                        isLoading: false,
                    })
                }, 3000);

            }


        } catch (e) {
            Alert.alert('Hata', 'Giriş işlemleri esnasında bir hata oluştu!');
        }
    }

    _callSignIn = () => {
        this.setState({
            isLoading: false,
            isRegistered: true
        })
    }

    getSnapshotBeforeUpdate(prevProps: Readonly<P>, prevState: Readonly<S>): SS | null {
        if(prevProps.token !== this.props.token){
            this._callSignIn();
            return true;
        }
        return false;
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        return snapshot;
    }

    render() {
        return (
            <NavigationContainer>

                {
                    this.state.isRegistered ?
                        <Stack.Navigator>
                            <Stack.Screen
                                name="ListPage"
                                component={ListPage}
                                options={{
                                    title: 'Seanslar',
                                    headerTitleStyle: {
                                        textAlign: 'center',
                                        width: Dimensions.get('window').width,
                                        color: Colors.WHITE,
                                        fontSize: 25
                                    },
                                    headerStyle: {
                                        backgroundColor: Colors.TRC_SECONDARY,
                                        borderBottomWidth: 0,
                                        elevation: 0,
                                        shadowColor: 'transparent'
                                    }
                                }}
                            />

                            <Stack.Screen
                                name="twoFingers"
                                component={FingerButtonsScreen}
                                options={{
                                    headerShown: false
                                }}
                            />


                        </Stack.Navigator>
                        /*<Tab.Navigator
                            initialRouteName="Home"
                            tabBarOptions={{
                                barStyle: {
                                    borderBottomWidth: 0,
                                    elevation: 0,
                                    shadowColor: 'transparent'
                                }
                            }}
                            barStyle={{backgroundColor: '#694fad'}}
                            tabBar={props => <MyTabBar {...props} />}
                        >
                            <Tab.Screen
                                name="User"
                                component={HomeScreen}
                                options={{
                                    tabBarLabel: 'Sayfam'
                                }}
                            />
                            <Tab.Screen
                                name="Home"
                                component={HomeScreen}
                                options={{
                                    tabBarLabel: 'Anasayfa'
                                }}
                            />
                            <Tab.Screen
                                name="Settings"
                                component={HomeScreen}
                                options={{
                                    tabBarLabel: 'Ayarlar'
                                }}
                            />
                        </Tab.Navigator>*/


                        :

                        <Stack.Navigator>
                            <Stack.Screen
                                name="Auth"
                                component={AuthScreen}
                                options={{
                                    headerShown: false
                                }}
                            />
                        </Stack.Navigator>
                }
            </NavigationContainer>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        token: state.authReducer.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => {
            dispatch({type: 'SET_TOKEN', payload: token});
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
