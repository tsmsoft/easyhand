import 'react-native-gesture-handler';
import React from 'react';
import Navigation from './Navigation';
import authReducer from './store/reducers/AuthReducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import commonReducer from './store/reducers/CommonReducer';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

const reducers = combineReducers({
    commonReducer: commonReducer,
    authReducer: authReducer,
});
const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

const App = () => {
    return (
        <Provider store={store}>
            <Navigation/>
        </Provider>
    );
};

export default App;
